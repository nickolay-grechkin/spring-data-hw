package com.bsa.springdata.user;

import com.bsa.springdata.role.Role;
import com.bsa.springdata.user.dto.UserDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    @Query("select u from User u where u.office.city = :city order by u.lastName asc ")
    List<User> findAllByCity(String city);

    List<User> findByLastNameLikeOrderByLastName(String lastName, Pageable pageable);

    List<User> findByExperienceGreaterThanEqual(int experience, Sort sort);

    @Query("select u from User u where  u.office.city = :city and u.team.room = :room order by u.lastName asc")
    List<User> findByRoomAndCity(String city, String room);

    void deleteUserByExperienceLessThan(int experience);

}
