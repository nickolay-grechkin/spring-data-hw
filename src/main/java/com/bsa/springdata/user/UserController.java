package com.bsa.springdata.user;

import com.bsa.springdata.project.ProjectService;
import com.bsa.springdata.role.RoleService;
import com.bsa.springdata.user.dto.CreateUserDto;
import com.bsa.springdata.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private ProjectService projectService;

    @GetMapping
    public List<UserDto> getUsers() {
        return userService.getUsers();
    }

    @GetMapping("/lastname")
    List<UserDto> getUsersByLastName(String lastname, @RequestParam(defaultValue = "0") int page,
                                     @RequestParam(defaultValue = "10") int size) {
        return userService.findByLastName(lastname, page, size);
    }

    @GetMapping("/city")
    List<UserDto> getUsersByLastName(String city) {
        return userService.findByCity(city);
    }

    @GetMapping("/experience")
    List<UserDto> getUserByExperience(String experience) {
        return userService.findByExperience(Integer.parseInt(experience));
    }

    @DeleteMapping("/deleteByExperience")
    int deleteUserByExperience(String experience) {
        return userService.deleteByExperience(Integer.parseInt(experience));
    }

    @DeleteMapping("/deleteRole")
    void deleteRole(String roleId) {
        roleService.deleteRole(roleId);
    }

    @GetMapping("/roomAndCity")
    List<UserDto> getUserByRoomAndCity(String city, String room) {
        return userService.findByRoomAndCity(city, room);
    }

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable UUID id) {
        return userService.getUserById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
    }

    @PostMapping
    public UUID createUser(@RequestBody CreateUserDto user) {
        return userService.createUser(user)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can not create user."));
    }
}
