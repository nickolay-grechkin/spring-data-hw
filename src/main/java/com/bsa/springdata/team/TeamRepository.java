package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {
    int countByTechnologyName(String newTechnology);

    Optional<Team> findByName(String hipsters_facebook_javaScript);

    @Modifying
    @Query("update Technology t set t.name = :newTechnologyName where t.name = :oldTechnologyName")
    void updateTechnologyName(String oldTechnologyName, String newTechnologyName);

    @Modifying
    @Query("update Team t set t.name = concat(t.name, '_', :prName,'_', :techName)  where t.name = :teamName")
    void normalizeName(String teamName, String prName, String techName);
}
