package com.bsa.springdata.team;

import com.bsa.springdata.project.Project;
import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "teams")
public class Team {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private String name;

    @Column
    private String room;

    @Column
    private String area;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "project_id")
    private Project project;

    @OneToMany(mappedBy = "team", fetch = FetchType.LAZY)
    private List<User> users;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "technology_id")
    private Technology technology;

    public static Team fromDto(CreateProjectRequestDto request, Project project, Technology technology) {
        return Team.builder()
                .name(request.getTeamName())
                .room(request.getTeamRoom())
                .area(request.getTeamArea())
                .project(project)
                .technology(technology)
                .build();
    }
}
