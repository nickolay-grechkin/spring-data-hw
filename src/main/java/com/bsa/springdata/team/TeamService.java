package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    @Transactional
    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        List<Team> teamsArr = teamRepository.findAll();
        for (Team t : teamsArr) {
            if (t.getUsers().size() < devsNumber) {
                teamRepository.updateTechnologyName(oldTechnologyName, newTechnologyName);
            }
        }
    }

    @Transactional
    public void normalizeName(String hipsters) {
        // TODO: Use a single query. You need to create a native query
        Optional<Team> team = teamRepository.findByName(hipsters);
        System.out.println(team.get().getProject().getName());
        teamRepository.normalizeName(hipsters, team.get().getProject().getName(), team.get().getTechnology().getName());
    }
}
