package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    @Transactional
    public List<ProjectDto> findTop5ByTechnology(String technology) {
        // TODO: Use single query to load data. Sort by number of developers in a project
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
        List<Project> projects = projectRepository.getProjectsByTechnology(technology, PageRequest.of(0, 5));
        sortByQuantityOfUsers(projects);
        return projects
                .stream()
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public Optional<ProjectDto> findTheBiggest() {
        // TODO: Use single query to load data. Sort by teams, developers, project name
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
        List<Project> projects = projectRepository.findAll();
        List<Project> tmpArray = new ArrayList<>();
        projects.sort(Comparator.comparingInt(p -> p.getTeams().size()));
        for (Project p : projects) {
            if (p.getTeams().size() == projects.get(projects.size() - 1).getTeams().size()) {
                tmpArray.add(p);
            }
        }
        if (tmpArray.size() == 1) {
            return Optional.of(ProjectDto.fromEntity(tmpArray.get(0)));
        }
        projects = findProjectsWithMaxUsers(tmpArray);
        if (projects.size() > 1) {
            projects.sort(Comparator.comparing(Project::getName));
            Collections.reverse(projects);
        }

        return Optional.of(ProjectDto.fromEntity(projects.get(0)));
    }

    @Transactional
    public List<ProjectSummaryDto> getSummary() {
        // TODO: Try to use native query and projection first. If it fails try to make as few queries as possible
        List<Project> projects = projectRepository.findAll();
        List<ProjectSummaryDto> summaryDto = new ArrayList<>();
        for (Project p : projects) {
            summaryDto.add(ProjectSummaryDto.fromEntity(p));
        }
        return summaryDto;
    }

    @Transactional
    public int getCountWithRole(String role) {
        System.out.println(projectRepository.getRolesSize());
        return 0;
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        // TODO: Use common JPARepository methods. Build entities in memory and then persist them
        Technology technology = new Technology();
        technology.setId(UUID.randomUUID());
        technology.setName(createProjectRequest.getTech());
        technology.setDescription(createProjectRequest.getProjectDescription());
        technology.setLink(createProjectRequest.getTechLink());
        var resultTech = technologyRepository.save(technology);
        Project project = new Project();
        project.setId(UUID.randomUUID());
        project.setName(createProjectRequest.getProjectName());
        project.setDescription(createProjectRequest.getProjectDescription());
        var resultProject = projectRepository.save(project);
        var team = Team.fromDto(createProjectRequest, resultProject, resultTech);
        teamRepository.save(team);
        return resultProject.getId();
    }

    public int getQuantityOfUsers(Project project) {
        int sum = 0;
        for (int i = 0; i < project.getTeams().size(); i++) {
            sum += project.getTeams().get(i).getUsers().size();
        }
        return sum;
    }

    public void sortByQuantityOfUsers(List<Project> list) {
        list.sort(Comparator.comparingInt(this::getQuantityOfUsers));
    }

    public List<Project> findProjectsWithMaxUsers(List<Project> list) {
        ArrayList<Project> maxProjects = new ArrayList<>();
        int max = 0;
        for (Project p : list) {
            if (getQuantityOfUsers(p) > max) {
                max = getQuantityOfUsers(p);
            }
        }
        for (Project p : list) {
            if (getQuantityOfUsers(p) == max) {
                maxProjects.add(p);
            }
        }
        return maxProjects;
    }
}
