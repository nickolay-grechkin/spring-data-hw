package com.bsa.springdata.project.dto;

import com.bsa.springdata.project.Project;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

// TODO: Use this interface when you make a projection from native query.
//  If you don't use native query replace this interface with a simple POJO
@Data
@Builder
public class ProjectSummaryDto {
    String name;
    long teamsNumber;
    long developersNumber;
    String technologies;

    public static ProjectSummaryDto fromEntity(Project project) {
        return ProjectSummaryDto
                .builder()
                .name(project.getName())
                .teamsNumber(project.getTeams().size())
                .developersNumber(getQuantityOfUsers(project))
                .technologies(getTechnologies(project))
                .build();
    }

    public static long getQuantityOfUsers(Project project) {
        int sum = 0;
        for (int i = 0; i < project.getTeams().size(); i++) {
            sum += project.getTeams().get(i).getUsers().size();
        }
        return sum;
    }

    public static String getTechnologies(Project project) {
        List<String> technologies = new ArrayList<>();
        String technologyName;
        for (int i = 0; i < project.getTeams().size(); i++) {
            technologyName = project.getTeams().get(i).getTechnology().getName();
            technologies.add(technologyName);
        }
        return String.join(",", technologies);
    }
}