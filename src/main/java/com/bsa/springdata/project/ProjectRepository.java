package com.bsa.springdata.project;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.relational.core.sql.In;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
    @Query("select p from Project p where p.id in (select t.project.id from Team t where t.technology.name = :technology)")
    List<Project> getProjectsByTechnology(String technology, Pageable pageable);

    @Query("select r.users.size from Role r where User.firstName = 'Jonty'")
    int getRolesSize();
}