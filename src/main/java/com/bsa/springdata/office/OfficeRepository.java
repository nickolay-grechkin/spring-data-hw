package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query("select o from Office o where o.id in (select u.office.id from User u where u.team.technology.name = :technology)")
    List<Office> getOfficeByTechnology(String technology);

    @Modifying
    @Query("update Office o set o.address = :newAddress where o.id in (select  u.office.id from User u where u.office.address = :oldAddress)")
    void updateOfficeAddress(String oldAddress, String newAddress);

    Office getOfficeByAddress(String address);
}
