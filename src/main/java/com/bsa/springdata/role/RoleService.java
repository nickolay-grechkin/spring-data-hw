package com.bsa.springdata.role;

import com.bsa.springdata.user.User;
import com.bsa.springdata.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;
    private UserRepository userRepository;

    @Transactional
    public void deleteRole(String roleCode) {
        roleRepository.deleteRoleByCode(roleCode);
    }
}
