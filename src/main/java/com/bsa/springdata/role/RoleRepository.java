package com.bsa.springdata.role;

import com.bsa.springdata.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    @Modifying
    @Query("delete from Role r where r.users.size = 0 ")
    void deleteRoleByCode(String roleCode);
}
